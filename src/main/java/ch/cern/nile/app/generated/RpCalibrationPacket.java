// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

package ch.cern.nile.app.generated;

import io.kaitai.struct.ByteBufferKaitaiStream;
import io.kaitai.struct.KaitaiStruct;
import io.kaitai.struct.KaitaiStream;
import java.io.IOException;

public class RpCalibrationPacket extends KaitaiStruct {
    public static RpCalibrationPacket fromFile(String fileName) throws IOException {
        return new RpCalibrationPacket(new ByteBufferKaitaiStream(fileName));
    }

    public RpCalibrationPacket(KaitaiStream _io) {
        this(_io, null, null);
    }

    public RpCalibrationPacket(KaitaiStream _io, KaitaiStruct _parent) {
        this(_io, _parent, null);
    }

    public RpCalibrationPacket(KaitaiStream _io, KaitaiStruct _parent, RpCalibrationPacket _root) {
        super(_io);
        this._parent = _parent;
        this._root = _root == null ? this : _root;
        _read();
    }
    private void _read() {
        this.devId = this._io.readU1();
        this.packageNum = this._io.readU2be();
        this.alarm = this._io.readU1();
        this.alarmCounts = this._io.readU4be();
        this.counts = this._io.readU4be();
        this.counts1hAgo = this._io.readU4be();
        this.counts2hAgo = this._io.readU4be();
        this.checkingTime = this._io.readU2be();
        this.checkingTime1hAgo = this._io.readU2be();
        this.checkingTime2hAgo = this._io.readU2be();
        this.shockCounts = this._io.readU4be();
        this.temperature = this._io.readU2be();
        this.unused = this._io.readU1();
        this.mon3v3 = this._io.readU2be();
        this.mon5 = this._io.readU2be();
        this.monVin = this._io.readU2be();
        this.exwdtc = this._io.readU2be();
    }
    private Double mon3v3Voltage;
    public Double mon3v3Voltage() {
        if (this.mon3v3Voltage != null)
            return this.mon3v3Voltage;
        double _tmp = (double) ((mon3v3() * 0.0008056640625));
        this.mon3v3Voltage = _tmp;
        return this.mon3v3Voltage;
    }
    private Double mon5Voltage;
    public Double mon5Voltage() {
        if (this.mon5Voltage != null)
            return this.mon5Voltage;
        double _tmp = (double) ((mon5() * 0.0008056640625));
        this.mon5Voltage = _tmp;
        return this.mon5Voltage;
    }
    private Double monVinVoltage;
    public Double monVinVoltage() {
        if (this.monVinVoltage != null)
            return this.monVinVoltage;
        double _tmp = (double) ((monVin() * 0.00185302734375));
        this.monVinVoltage = _tmp;
        return this.monVinVoltage;
    }
    private int devId;
    private int packageNum;
    private int alarm;
    private long alarmCounts;
    private long counts;
    private long counts1hAgo;
    private long counts2hAgo;
    private int checkingTime;
    private int checkingTime1hAgo;
    private int checkingTime2hAgo;
    private long shockCounts;
    private int temperature;
    private int unused;
    private int mon3v3;
    private int mon5;
    private int monVin;
    private int exwdtc;
    private RpCalibrationPacket _root;
    private KaitaiStruct _parent;
    public int devId() { return devId; }
    public int packageNum() { return packageNum; }
    public int alarm() { return alarm; }
    public long alarmCounts() { return alarmCounts; }
    public long counts() { return counts; }
    public long counts1hAgo() { return counts1hAgo; }
    public long counts2hAgo() { return counts2hAgo; }
    public int checkingTime() { return checkingTime; }
    public int checkingTime1hAgo() { return checkingTime1hAgo; }
    public int checkingTime2hAgo() { return checkingTime2hAgo; }
    public long shockCounts() { return shockCounts; }
    public int temperature() { return temperature; }
    public int unused() { return unused; }
    public int mon3v3() { return mon3v3; }
    public int mon5() { return mon5; }
    public int monVin() { return monVin; }
    public int exwdtc() { return exwdtc; }
    public RpCalibrationPacket _root() { return _root; }
    public KaitaiStruct _parent() { return _parent; }
}
