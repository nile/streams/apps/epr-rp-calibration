package ch.cern.nile.app.generated;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Map;

import com.google.gson.JsonElement;

import org.junit.jupiter.api.Test;

import ch.cern.nile.common.exceptions.DecodingException;
import ch.cern.nile.kaitai.decoder.KaitaiPacketDecoder;
import ch.cern.nile.test.utils.TestUtils;

public class RpCalibrationPacketTest {
    private static final JsonElement DATA_FRAME =
            TestUtils.getDataAsJsonElement("3QABAQAAADIAAAAyAAAAAQAAAAIOEA4QDeMAAAAQASIAD/8G7goMAAI=");
    private static final JsonElement DATA_FRAME_NO_RSSI_NO_SNR =
            TestUtils.getDataAsJsonElement("3QABAQAAADIAAAAyAAAAAQAAAAIOEA4QDeMAAAAQASIAD/8G7goMAAI=");

    @Test
    void givenDataFrame_whenDecoding_thenCorrectlyDecodesMessageData() throws DecodingException {
        Map<String, Object> packet = KaitaiPacketDecoder.decode(DATA_FRAME, RpCalibrationPacket.class);
        assertEquals(2572, packet.get("monVin"));
        assertEquals(4.765986328125, packet.get("monVin_voltage"));
        assertEquals(3.2991943359375, packet.get("mon3v3_voltage"));
        assertEquals(2, packet.get("exwdtc"));
        assertEquals(50L, packet.get("counts"));
        assertEquals(4095, packet.get("mon3v3"));
        assertEquals(221, packet.get("dev_ID"));
        assertEquals(1.429248046875, packet.get("mon5_voltage"));
        assertEquals(16L, packet.get("shock_counts"));
        assertEquals(1, packet.get("alarm"));
        assertEquals(290, packet.get("temperature"));
        assertEquals(50L, packet.get("alarm_counts"));
        assertEquals(1774, packet.get("mon5"));
        assertEquals(3600, packet.get("checking_time"));
        assertEquals(1, packet.get("package_num"));
    }

    @Test
    void givenDataFrameNoRssiNoSnr_whenDecoding_thenCorrectlyDecodesMessageData() throws DecodingException {
        Map<String, Object> packet = KaitaiPacketDecoder.decode(DATA_FRAME_NO_RSSI_NO_SNR, RpCalibrationPacket.class);

        assertNull(packet.get("rssi_gw_0"));
        assertNull(packet.get("rssi_gw_1"));
        assertNull(packet.get("snr_gw_0"));
        assertNull(packet.get("snr_gw_1"));
    }

}
