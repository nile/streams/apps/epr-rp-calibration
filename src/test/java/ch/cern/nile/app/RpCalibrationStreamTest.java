package ch.cern.nile.app;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.text.ParseException;
import java.time.DateTimeException;

import com.google.gson.JsonObject;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import ch.cern.nile.test.utils.StreamTestBase;
import ch.cern.nile.test.utils.TestUtils;

public class RpCalibrationStreamTest extends StreamTestBase {

    private static final JsonObject DATA_FRAME = TestUtils.loadRecordAsJson("data/data_frame.json");
    private static final JsonObject DATA_FRAME_NO_RSSI_NO_SNR =
            TestUtils.loadRecordAsJson("data/data_frame_no_rssi_no_snr.json");
    private static final JsonObject DATA_FRAME_PARSE_ERROR =
            TestUtils.loadRecordAsJson("data/data_frame_parse_error.json");
    private static final JsonObject DATA_FRAME_NULL_POINTER_TIME =
            TestUtils.loadRecordAsJson("data/data_frame_null_pointer_time.json");

    @Override
    public RpCalibrationStream createStreamInstance() {
        return new RpCalibrationStream();
    }

    @Test
    void givenDataFrame_whenDecoding_whenDecoding_thenCorrectOutputRecordIsCreated
            () {
        pipeRecord(DATA_FRAME);

        final ProducerRecord<String, JsonObject> outputRecord = readRecord();

        assertEquals(2572, outputRecord.value().get("monVin").getAsInt());
        assertEquals(4.765986328125, outputRecord.value().get("monVin_voltage").getAsDouble());
        assertEquals(3.2991943359375, outputRecord.value().get("mon3v3_voltage").getAsDouble());
        assertEquals(2, outputRecord.value().get("exwdtc").getAsInt());
        assertEquals(8, outputRecord.value().get("fPort").getAsInt());
        assertEquals(50, outputRecord.value().get("counts").getAsInt());
        assertEquals(4095, outputRecord.value().get("mon3v3").getAsInt());
        assertEquals(221, outputRecord.value().get("dev_ID").getAsInt());
        assertEquals(1.429248046875, outputRecord.value().get("mon5_voltage").getAsDouble());
        assertEquals("rp-wmon-44", outputRecord.value().get("device_name").getAsString());
        assertEquals(16, outputRecord.value().get("shock_counts").getAsInt());
        assertEquals(1, outputRecord.value().get("alarm").getAsInt());
        assertEquals(290, outputRecord.value().get("temperature").getAsInt());
        assertEquals(50, outputRecord.value().get("alarm_counts").getAsInt());
        assertEquals(1774, outputRecord.value().get("mon5").getAsInt());
        assertEquals(3600, outputRecord.value().get("checking_time").getAsInt());
        assertEquals(1, outputRecord.value().get("package_num").getAsInt());
        assertEquals(-108, outputRecord.value().get("rssi_gw_0").getAsInt());
        assertEquals(-107, outputRecord.value().get("rssi_gw_1").getAsInt());
        assertEquals(5, outputRecord.value().get("snr_gw_0").getAsInt());
        assertEquals(-2, outputRecord.value().get("snr_gw_1").getAsInt());
        assertNotNull(outputRecord.value().get("timestamp"), "Timestamp is null");
    }


    @Test
    void givenDataFrameNoRssiNoSnr_whenDecoding_whenDecoding_thenCorrectOutputRecordIsCreated
            () {
        pipeRecord(DATA_FRAME_NO_RSSI_NO_SNR);

        final ProducerRecord<String, JsonObject> outputRecord = readRecord();

        assertNull(outputRecord.value().get("rssi_gw_0"));
        assertNull(outputRecord.value().get("rssi_gw_1"));
        assertNull(outputRecord.value().get("snr_gw_0"));
        assertNull(outputRecord.value().get("snr_gw_1"));
    }

    @Test
    @Disabled
    void givenWrongMessageFormat_whenDecoding_thenParseExceptionThrown() {
        assertThrows(ParseException.class, () -> pipeRecord(DATA_FRAME_PARSE_ERROR));
    }

    @Test
    @Disabled
    void givenMessageWithNoTime_whenDecoding_thenDateTimeExceptionThrown() {
        assertThrows(DateTimeException.class, () -> pipeRecord(DATA_FRAME_NULL_POINTER_TIME));
    }

}
