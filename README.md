# LoRa Calibration

## Overview

### Owners & Administrators

| Title       | Details                                                                     |
|-------------|-----------------------------------------------------------------------------|
| **Section** | [BE-CEM-EPR](https://phonebook.cern.ch/search?q=BE-CEM-EPR)                 |
| **E-group** | -                                                                           |
| **People**  | [Alessandro Zimmaro](https://phonebook.cern.ch/search?q=Alessandro+Zimmaro) |

### Kafka Topics

| Environment    | Topic Name                                                                                                              |
|----------------|-------------------------------------------------------------------------------------------------------------------------|
| **Production** | [lora-rp-calibration](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-rp-calibration)                 |
| **Production** | [lora-rp-calibration-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-rp-calibration-decoded) |
| **QA**         | [lora-rp-calibration](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-rp-calibration)                 |
| **QA**         | [lora-rp-calibration-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-rp-calibration-decoded) |

### Configuration

| Title                        | Details                                                                                                    |
|------------------------------|------------------------------------------------------------------------------------------------------------|
| **Application Name**         | cern-rp-wmon-calibration                                                                                   |
| **Configuration Repository** | [app-configs/lora-rp-calibration](https://www.gitlab.cern.ch/nile/streams/app-configs/lora-rp-calibration) |

### Technologies

- LoRa
